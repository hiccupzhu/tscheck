/*
 *  mbuf.h
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#ifndef _M_BUF_H_
#define _M_BUF_H_

#include <glib-object.h>

#define M_TYPE_BUF               (m_buf_get_type ())
#define M_BUF(object)            (G_TYPE_CHECK_INSTANCE_CAST ((object), M_TYPE_BUF, MBuf))
#define M_IS_BUF(object)         (G_TYPE_CHECK_INSTANCE_TYPE ((object), M_TYPE_BUF))
#define M_BUF_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), M_TYPE_BUF, MBufClass))
#define M_IS_BUF_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), M_TYPE_BUF))
#define M_BUF_GET_CLASS(object)  (G_TYPE_INSTANCE_GET_CLASS ((object), M_TYPE_BUF, MBufClass))

typedef struct _MBuf          MBuf;
typedef struct _MBufClass     MBufClass;
typedef struct _MBufPrivate   MBufPrivate;

struct _MBuf {
    GObject parent;
    MBufPrivate *priv;

    guint8 *data;
    gint    alloced_size;
    gint    size;
};

struct _MBufClass {
    GObjectClass parent_class;
};

GType m_buf_get_type (void);
MBuf *m_buf_new (void);
MBuf *m_buf_new_alloc (int size);
void m_buf_do_action (MBuf *b);

#endif
