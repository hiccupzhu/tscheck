/*
 * tsreader.c
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */
#include <stdlib.h>
#include "gdef.h"
#include "tsreader.h"

G_DEFINE_TYPE (TsReader, ts_reader, G_TYPE_OBJECT)

struct _TsReaderPrivate
{
    int temp;
};

#define REFIX_READER_GET_PRIVATE(obj) \
    (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TS_TYPE_READER, TsReaderPrivate))

static void ts_reader_dispose(GObject *object);
static void ts_reader_finalize(GObject *object);
static void ts_reader_constructed (GObject *object);

//////////////////////////////////////////////////////////////////////////////
static void  ts_reader_get_packet(u_char * arg, const struct pcap_pkthdr * pkthdr, const u_char * packet);
static void* ts_reader_capture_loop(void *data);



static
void ts_reader_class_init (TsReaderClass *klass){
    GObjectClass *base_class = G_OBJECT_CLASS (klass);

    av_printf("\n");

    base_class->constructed  = ts_reader_constructed;
    base_class->dispose      = ts_reader_dispose;
    base_class->finalize     = ts_reader_finalize;

    g_type_class_add_private (klass, sizeof(TsReaderPrivate));
}

static
void ts_reader_init (TsReader *r){
    TsReaderPrivate *priv;

    av_printf("\n");

    r->priv = priv = REFIX_READER_GET_PRIVATE (r);

    r->device = NULL;
    r->cap_handle = -1;
    r->qlock = g_mutex_new();
    r->queue = g_queue_new();
    r->quit = FALSE;
}

static void
ts_reader_constructed (GObject *object)
{
    TsReader *r = TS_READER(object);
}

static
void ts_reader_dispose(GObject *object){
    TsReader *r = TS_READER(object);
    av_printf("\n");

    G_OBJECT_CLASS(ts_reader_parent_class)->dispose(object);
}

static
void ts_reader_finalize(GObject *object){
    TsReader *r = TS_READER(object);
    av_printf("\n");

    SAFE_GMUTEX_FREE(r->qlock);
    SAFE_GQUEUE_FREE(r->queue);

    G_OBJECT_CLASS(ts_reader_parent_class)->finalize(object);
}

TsReader *
ts_reader_new (void)
{
    return g_object_new(TS_TYPE_READER, NULL);
}

static void
ts_reader_get_packet(u_char * arg, const struct pcap_pkthdr * pkthdr, const u_char * packet)
{
    TsReader *r = (TsReader*)arg;
    MBuf *buf;
    static const int UDP_HD_SIZE = 42;

//    av_print_int(pkthdr->len);

    buf = m_buf_new_alloc(pkthdr->len - UDP_HD_SIZE);
    memcpy(buf->data, packet + UDP_HD_SIZE, pkthdr->len - UDP_HD_SIZE);
    buf->size = pkthdr->len - UDP_HD_SIZE;


    g_mutex_lock(r->qlock);
    g_queue_push_tail(r->queue, buf);
    g_mutex_unlock(r->qlock);

}

static void*
ts_reader_capture_loop(void *data)
{
    TsReader *r = (TsReader*)data;

    pcap_loop(r->device, -1, ts_reader_get_packet, (u_char*)r);

    return NULL;
}

gboolean
ts_reader_open (TsReader *r, const char* dev_name, const char* option)
{
    int ret;
    char errBuf[PCAP_ERRBUF_SIZE];
    struct bpf_program bpf;

    g_return_val_if_fail(TS_IS_READER(r), FALSE);

    r->quit = 0;

    r->device = pcap_open_live(dev_name, 1500, 1, 0, errBuf);
    if(!r->device){
        g_print("*** error: pcap_open_live(): %s\n", errBuf);
        return FALSE;
    }

    ret = pcap_compile(r->device, &bpf, option, 1, 0);
    if(ret != 0){
        g_print("*** error pcap_compile FAILED!\n");
        return FALSE;
    }
    ret = pcap_setfilter(r->device, &bpf);
    if(ret != 0){
        g_print("*** error pcap_setfilter FAILED! ret=%d\n", ret);
        return FALSE;
    }
    //    pcap_loop(r->device, -1, gst_tcp_dump_src_get_packet, (u_char*)r);

    ret = pthread_create(&r->cap_handle, NULL, ts_reader_capture_loop, r );
    if(ret != 0){
        g_print("*** error pthread_create FAILED! ret=%d\n", ret);
        return FALSE;
    }

    return TRUE;
}

gboolean
ts_reader_close (TsReader *r)
{
    g_return_val_if_fail(TS_IS_READER(r), FALSE);

    return TRUE;
}

MBuf*
ts_reader_read(TsReader *r)
{
    MBuf* buf = NULL;

    if(g_queue_get_length(r->queue) > 0){
        g_mutex_lock(r->qlock);
        buf = g_queue_pop_head(r->queue);
        g_mutex_unlock(r->qlock);
    }

    return buf;
}
