/*
 * mbyte.c
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#include <string.h>
#include "mbyte.h"
#include "gdef.h"

G_DEFINE_TYPE (MByte, m_byte, G_TYPE_OBJECT)

struct _MBytePrivate
{
    int temp;
};

#define REFIX_BYTE_GET_PRIVATE(obj) \
        (G_TYPE_INSTANCE_GET_PRIVATE ((obj), M_TYPE_BYTE, MBytePrivate))

static void m_byte_dispose(GObject *object);
static void m_byte_finalize(GObject *object);
static void m_byte_constructed (GObject *object);

static
void m_byte_class_init (MByteClass *klass){
    GObjectClass *base_class = G_OBJECT_CLASS (klass);

    av_printf("\n");

    base_class->constructed  = m_byte_constructed;
    base_class->dispose      = m_byte_dispose;
    base_class->finalize     = m_byte_finalize;

    g_type_class_add_private (klass, sizeof(MBytePrivate));
}

static
void m_byte_init (MByte *b){
    MBytePrivate *priv;

//    av_printf("\n");

    b->priv = priv = REFIX_BYTE_GET_PRIVATE (b);

    b->mBufPtr = NULL;
    b->mBuf = NULL;
    b->mBufEnd = NULL;
}

static void
m_byte_constructed (GObject *object)
{
    MByte *b = M_BYTE(object);
}

static
void m_byte_dispose(GObject *object){
    MByte *b = M_BYTE(object);
//    av_printf("\n");

    G_OBJECT_CLASS(m_byte_parent_class)->dispose(object);
}

static
void m_byte_finalize(GObject *object){
    MByte *b = M_BYTE(object);
    av_printf("\n");
    G_OBJECT_CLASS(m_byte_parent_class)->finalize(object);
}

MByte *
m_byte_new (void)
{
    return g_object_new(M_TYPE_BYTE, NULL);
}

void m_byte_open(MByte *b, const guint8 *buffer, const int size) {
    b->mBuf = buffer;
    b->mBufPtr = buffer;
    b->mBufEnd = b->mBuf + size;
}

void m_byte_close(MByte *b) {
    b->mBufPtr = NULL;
    b->mBuf = NULL;
    b->mBufEnd = NULL;
}

int m_byte_rbuffer(MByte *b, void* buf, const int length) {
    int len = 0;
    if (length > 0) {
        if (b->mBufEnd >= (b->mBufPtr + length)) {
            len = length;
            memcpy(buf, b->mBufPtr, len);
        } else {
            len = b->mBufEnd - b->mBufPtr;
            len = MMAX(0, len);
            memcpy(buf, b->mBufPtr, len);
        }
        b->mBufPtr += len;
    } else if (length == 0) {
        len = 0;
    } else {
        len = MFAILED;
    }

    return len;
}

int m_byte_r8(MByte *b) {
    int val = 0;
    if (b->mBufPtr < b->mBufEnd)
        val = *b->mBufPtr++;
    return val;
}

int m_byte_seek_to(MByte *b, const guint64 offset) {
    int res = MFAILED;
    const guint8* des = b->mBuf + offset;
    if (des >= b->mBuf && des <= b->mBufEnd) {
        b->mBufPtr = des;
        res = MSUCCESS;
    }
    return res;
}

void m_byte_seek_fwd(MByte *b, int len) {
    b->mBufPtr = MMIN((b->mBufPtr + len), b->mBufEnd);
}

void m_byte_seek_back(MByte *b, int len) {
    b->mBufPtr = MMAX((b->mBufPtr - len), b->mBuf);
}

void m_byte_seek(MByte *b, int len, int mode) {
    if(mode == SEEK_SET){
        if(len >= 0){
            b->mBufPtr = MMIN(b->mBuf + len, b->mBufEnd);
        }
    }else if(mode == SEEK_CUR){
        b->mBufPtr = MMIN(MMAX(b->mBuf, (b->mBufPtr + len)), b->mBufEnd);
    }

}

void m_byte_skip(MByte *b, int len){
    m_byte_seek(b, len, SEEK_CUR);
}

guint64 m_byte_tell(MByte *b) {
    return b->mBufPtr - b->mBuf;
}

int m_byte_eof(MByte *b) {
    return (b->mBufPtr >= b->mBufEnd);
}

int m_byte_find(MByte *b, guint32* id, const guint32 wanted, const guint32 mask) {
    int res = MFAILED;
    guint32 last_found = 0xFFFFFFFF;
    *id = 0;

    while (!m_byte_eof(b)) {
        last_found = (last_found << 8) | (m_byte_r8(b));
        if ((last_found & mask) == wanted) {
            res = 0;
            *id = last_found;
            break;
        }
    }
    if (m_byte_eof(b))
        res = MEOS;

    return res;
}

int m_byte_rb16(MByte *b) {
    int val = 0;
    val = m_byte_r8(b) << 8;
    val |= m_byte_r8(b);
    return val;
}

int m_byte_rb24(MByte *b) {
    int val = 0;
    val = m_byte_rb16(b) << 8;
    val |= m_byte_r8(b);
    return val;
}

int m_byte_rb32(MByte *b) {
    int val = 0;
    val = m_byte_rb16(b) << 16;
    val |= m_byte_rb16(b);
    return val;
}

int m_byte_get_buffer_size(MByte *b) {
    return b->mBufEnd - b->mBuf;
}

int m_byte_get_data_size(MByte *b) {
    return b->mBufPtr - b->mBuf;
}

int m_byte_get_remained_size(MByte *b){
    return b->mBufEnd - b->mBufPtr;
}


