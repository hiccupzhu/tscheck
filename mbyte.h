/*
 *  mbyte.h
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#ifndef _M_BYTE_H_
#define _M_BYTE_H_

#include <glib-object.h>

#define M_TYPE_BYTE               (m_byte_get_type ())
#define M_BYTE(object)            (G_TYPE_CHECK_INSTANCE_CAST ((object), M_TYPE_BYTE, MByte))
#define M_IS_BYTE(object)         (G_TYPE_CHECK_INSTANCE_TYPE ((object), M_TYPE_BYTE))
#define M_BYTE_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), M_TYPE_BYTE, MByteClass))
#define M_IS_BYTE_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), M_TYPE_BYTE))
#define M_BYTE_GET_CLASS(object)  (G_TYPE_INSTANCE_GET_CLASS ((object), M_TYPE_BYTE, MByteClass))

typedef struct _MByte          MByte;
typedef struct _MByteClass     MByteClass;
typedef struct _MBytePrivate   MBytePrivate;

struct _MByte {
    GObject parent;

    MBytePrivate *priv;

    const guint8 * mBuf;
    const guint8 * mBufPtr;
    const guint8 * mBufEnd;
};

struct _MByteClass {
    GObjectClass parent_class;
};

GType m_byte_get_type (void);
MByte *m_byte_new (void);

void    m_byte_open(MByte *b, const guint8 *buffer, const int size);
void    m_byte_close(MByte *b);
int     m_byte_rbuffer(MByte *b, void* buf, const int length);
int     m_byte_r8(MByte *b);
int     m_byte_seek_to(MByte *b, const guint64 offset);
void    m_byte_seek_fwd(MByte *b, int len);
void    m_byte_seek_back(MByte *b, int len);
void    m_byte_seek(MByte *b, int len, int mode);
void    m_byte_skip(MByte *b, int len);
guint64  m_byte_tell(MByte *b);
int     m_byte_eof(MByte *b);
int     m_byte_find(MByte *b, guint32* id, const guint32 wanted, const guint32 mask);
int     m_byte_rb16(MByte *b);
int     m_byte_rb24(MByte *b);
int     m_byte_rb32(MByte *b);
int     m_byte_get_buffer_size(MByte *b);
int     m_byte_get_data_size(MByte *b);
int     m_byte_get_remained_size(MByte *b);

#endif
