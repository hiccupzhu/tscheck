/*
 *  tsreader.h
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#ifndef _TS_READER_H_
#define _TS_READER_H_

#include <glib-object.h>
#include <pcap.h>
#include <pthread.h>

#include "mbuf.h"

#define TS_TYPE_READER               (ts_reader_get_type ())
#define TS_READER(object)            (G_TYPE_CHECK_INSTANCE_CAST ((object), TS_TYPE_READER, TsReader))
#define TS_IS_READER(object)         (G_TYPE_CHECK_INSTANCE_TYPE ((object), TS_TYPE_READER))
#define TS_READER_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), TS_TYPE_READER, TsReaderClass))
#define TS_IS_READER_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), TS_TYPE_READER))
#define TS_READER_GET_CLASS(object)  (G_TYPE_INSTANCE_GET_CLASS ((object), TS_TYPE_READER, TsReaderClass))

typedef struct _TsReader          TsReader;
typedef struct _TsReaderClass     TsReaderClass;
typedef struct _TsReaderPrivate   TsReaderPrivate;

struct _TsReader {
    GObject parent;

    pcap_t * device;
    pthread_t cap_handle;

    int quit;

    GMutex *qlock;
    GQueue *queue;

    TsReaderPrivate *priv;
};

struct _TsReaderClass {
    GObjectClass parent_class;
};

GType ts_reader_get_type (void);
TsReader *ts_reader_new (void);
gboolean ts_reader_open  (TsReader *r, const char* dev_name, const char* option);
gboolean ts_reader_close (TsReader *r);
MBuf*    ts_reader_read(TsReader *r);


#endif
