/*
 * mbuf.c
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */
#include "gdef.h"
#include "mbuf.h"

G_DEFINE_TYPE (MBuf, m_buf, G_TYPE_OBJECT)

struct _MBufPrivate
{
    int temp;
};

#define REFIX_BUF_GET_PRIVATE(obj) \
    (G_TYPE_INSTANCE_GET_PRIVATE ((obj), M_TYPE_BUF, MBufPrivate))

static void m_buf_dispose(GObject *object);
static void m_buf_finalize(GObject *object);
static void m_buf_constructed (GObject *object);

static
void m_buf_class_init (MBufClass *klass){
    GObjectClass *base_class = G_OBJECT_CLASS (klass);

//    av_printf("\n");

    base_class->constructed  = m_buf_constructed;
    base_class->dispose      = m_buf_dispose;
    base_class->finalize     = m_buf_finalize;

    g_type_class_add_private (klass, sizeof(MBufPrivate));
}

static
void m_buf_init (MBuf *b){
    MBufPrivate *priv;

    b->priv = priv = REFIX_BUF_GET_PRIVATE (b);

    b->size = 0;
    b->data = NULL;
    b->alloced_size = 0;
}

static void
m_buf_constructed (GObject *object)
{
    MBuf *b = M_BUF(object);
}

static
void m_buf_dispose(GObject *object){
    MBuf *b = M_BUF(object);
//    av_printf("\n");

    G_OBJECT_CLASS(m_buf_parent_class)->dispose(object);
}

static
void m_buf_finalize(GObject *object){
    MBuf *b = M_BUF(object);
//    av_printf("\n");

    SAFE_FREE(b->data);
    b->alloced_size = 0;
    b->size = 0;

    G_OBJECT_CLASS(m_buf_parent_class)->finalize(object);
}

MBuf *
m_buf_new (void)
{
    return g_object_new(M_TYPE_BUF, NULL);
}

MBuf *
m_buf_new_alloc (int size)
{
    MBuf *b;
    b = g_object_new(M_TYPE_BUF, NULL);
    b->data = malloc(size);
    b->alloced_size = size;
    b->size = 0;
    return b;
}

void
m_buf_do_action (MBuf *b)
{
    g_return_if_fail(M_IS_BUF(b));

}

