/*
 * tsparser.c
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#include <assert.h>
#include <malloc.h>
#include "gdef.h"
#include "tsparser.h"

G_DEFINE_TYPE (TsParser, ts_parser, G_TYPE_OBJECT)

struct _TsParserPrivate
{
    int temp;
};

#define REFIX_PARSER_GET_PRIVATE(obj) \
    (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TS_TYPE_PARSER, TsParserPrivate))

static void ts_parser_dispose(GObject *object);
static void ts_parser_finalize(GObject *object);
static void ts_parser_constructed (GObject *object);

static
void ts_parser_class_init (TsParserClass *klass){
    GObjectClass *base_class = G_OBJECT_CLASS (klass);

    av_printf("\n");

    base_class->constructed  = ts_parser_constructed;
    base_class->dispose      = ts_parser_dispose;
    base_class->finalize     = ts_parser_finalize;

    g_type_class_add_private (klass, sizeof(TsParserPrivate));
}

static
void ts_parser_init (TsParser *ts){
    TsParserPrivate *priv;

    av_printf("\n");
    ts->priv = priv = REFIX_PARSER_GET_PRIVATE (ts);

    ts->b = m_byte_new();
    ts->mPacketSize = 0;
    ts->nb_dts = 0;
    ts->nb_pts = 0;
    ts->nb_pcr = 0;
//    ts->pts = g_array_new(FALSE, TRUE, sizeof(guint64));
//    ts->dts = g_array_new(FALSE, TRUE, sizeof(guint64));
//    ts->pcr = g_array_new(FALSE, TRUE, sizeof(guint64));
}

static void
ts_parser_constructed (GObject *object)
{
    TsParser *ts = TS_PARSER(object);
}

static
void ts_parser_dispose(GObject *object){
    TsParser *ts = TS_PARSER(object);
    av_printf("\n");

//    SAFE_GOBJECT_UNREF(ts->pts);
//    SAFE_GOBJECT_UNREF(ts->dts);
//    SAFE_GOBJECT_UNREF(ts->pcr);

    G_OBJECT_CLASS(ts_parser_parent_class)->dispose(object);
}

static
void ts_parser_finalize(GObject *object){
    TsParser *ts = TS_PARSER(object);
    av_printf("\n");

    g_object_unref(ts->b);

    G_OBJECT_CLASS(ts_parser_parent_class)->finalize(object);
}

TsParser *
ts_parser_new (void)
{
    return g_object_new(TS_TYPE_PARSER, NULL);
}

int
ts_parser_write(TsParser *ts, MBuf *buf)
{
    int res, i, ch, count;
    guint8 *p = buf->data;
    MByte *b = ts->b;
    TS_packet_header thd = ts->tshd;

    if(ts->mPacketSize == 0){
        ts_parser_adjust_packet_size(ts, buf->data, buf->size, &ts->mPacketSize);
    }
    ts_parser_clear(ts);

    count = buf->size / ts->mPacketSize;

    for(i = 0; i < count ; i++){
        m_byte_open(b, p, ts->mPacketSize);
//        res = ts_parser_anchor_start(ts, b, ts->mPacketSize); if(!res) { continue;}
        ts_parser_adjust_TSPacketHeader(ts, b, &thd);

        assert(thd.sync_byte == 0x47);

        switch(thd.PID){
        case TS_PID_PAT:
            //??  pointer field 1 Byte
//            av_printf("## PAT\n");
            ch = m_byte_r8(b);
            assert(ch == 0);
            ts_parser_adjust_PAT(ts, b, &thd);
            continue;
            break;
        case TS_PID_CAT:
        case TS_PID_TSDT:
        case TS_PID_NIT_ST:
        case TS_PID_SDT_BAT_ST:
        case TS_PID_EIT_ST:
        case TS_PID_RST_ST:
        case TS_PID_TDT_TOT_ST:
        case TS_PID_DIT:
        case TS_PID_SIT:
        case TS_PID_NULL:
            continue;
            break;
        default:
            //do nothing
            break;
        }

        if(ts_parser_isPMT_PID(ts, thd.PID)){
            //??  pointer field 1 Byte
//            av_printf("## PMT\n");
            ch = m_byte_r8(b);
            assert(ch == 0);
            ts_parser_adjust_PMT(ts, b, &thd);
        }else if(ts_parser_isPES_PID(ts, thd.PID)){
//            av_printf("## PES\n");
            ts_parser_adjust_PES(ts, b, &thd);
        }else{
//            av_print("UNKNOWN PID=%d[0x%X]\n", thd.PID, thd.PID);
        }

        ts->tshd = thd;
        p += ts->mPacketSize;
    }

    assert(count == 7);
    return 0;
}

int
ts_parser_clear(TsParser *ts)
{
    ts->nb_dts = 0;
    ts->nb_pcr = 0;
    ts->nb_pts = 0;

    return 0;
}

int ts_parser_adjust_TSPacketHeader(TsParser *ts, MByte *b, TS_packet_header *tshd){
    guint32 bt = m_byte_r8(b);
    tshd->sync_byte                       = bt;
    bt = m_byte_r8(b);
    tshd->transport_error_indicator       = bt >> 7;
    tshd->payload_unit_start_indicator    = bt >> 6 & 0x01;
    tshd->transport_priority              = bt >> 5 & 0x01;
    tshd->PID                             = (bt & 0x1F) << 8 | m_byte_r8(b);
    bt = m_byte_r8(b);
    tshd->transport_scrambling_control    = bt >> 6;
    tshd->adaption_field_control          = bt >> 4 & 0x03;
    tshd->continuity_counter              = bt & 0x03;
    return MOK;
}

int
ts_parser_adjust_packet_size(TsParser *ts, const guint8* buf, int size, int* pktSize)
{
    guint8 ch = 0;
    MByte *b = ts->b;
    m_byte_open(b, buf, size);
    while(TRUE){
        ch = m_byte_r8(b);
        if(ch == 0x47){
            m_byte_skip(b, 188 - 1);
            ch = m_byte_r8(b);
            if(ch == 0x47){
                *pktSize = 188;
                break;
            }else{
                m_byte_skip(b, 192 - 188 - 1);
                ch = m_byte_r8(b);
                if(ch == 0x47){
                    *pktSize = 192;
                }else{
                    *pktSize = 204;
                }
                break;
            }
        }
    }

    return 0;
}

gboolean
ts_parser_has_valid_PMT(TsParser *ts)
{
    int i;
    if(ts->nb_pmts <= 0) return FALSE;
    for(i = 0; i < ts->nb_pmts; i++){
        if(ts->pmts[i].nb_es <= 0){ break; }
    }
    return (i == ts->nb_pmts);
}

gboolean
ts_parser_anchor_start(TsParser *ts, MByte *b, int pktSize)
{
    gboolean found = FALSE;
    guint8 ch = 0;

//    av_print("data_size=%d pktSize=%d\n", m_byte_get_buffer_size(b), pktSize);

    while(!m_byte_eof(b)){
        ch = m_byte_r8(b);
        if(ch == 0x47){
            m_byte_skip(b, pktSize - 1);
            if(m_byte_eof(b)){
                m_byte_seek(b, -(pktSize + 1), SEEK_CUR);
                found = TRUE;
                break;
            }
            ch = m_byte_r8(b);
            if(ch == 0x47 ){
                m_byte_seek(b, -(pktSize + 1), SEEK_CUR);
                found = TRUE;
                break;
            }else{
                if(m_byte_eof(b)){
                    break;
                }else{
                    m_byte_seek(b, -(pktSize), SEEK_CUR);
                    continue;
                }
            }
        }
    }

    return found;
}

int
ts_parser_adjust_PAT(TsParser *ts, MByte *b, TS_packet_header* hd)
{
    guint32 bt;
    int n;
    TS_PAT *pat = &ts->pat;

    bt = 0;
    //3Bytes
    pat->table_id                    = m_byte_r8(b);
    bt = m_byte_r8(b);
    pat->section_syntax_indicator    = bt >> 7;
    pat->zero                        = bt >> 6 & 0x1;
    pat->reserved_1                  = bt >> 4 & 0x3;
    pat->section_length              = (bt & 0x0F) << 8 | m_byte_r8(b);

    //5Bytes
    pat->transport_stream_id         = m_byte_r8(b) << 8 | m_byte_r8(b);
    bt = m_byte_r8(b);
    pat->reserved_2                  = bt >> 6;
    pat->version_number              = bt >> 1 &  0x1F;
    pat->current_next_indicator      = (bt << 7) >> 7;
    pat->section_number              = m_byte_r8(b);
    pat->last_section_number         = m_byte_r8(b);


    //5 = current_ptr - section_length; 4 = CRC32
    gboolean displayInfo = FALSE;
    int program_count = (pat->section_length - 5 - 4) / 4;
    if(program_count > ts->nb_pmts){
        SAFE_FREE(ts->pmts);
        ts->pmts = (TS_MPMT*)calloc(program_count, sizeof(TS_MPMT));
        ts->nb_pmts = program_count;
        displayInfo = TRUE;
    }

    for (n = 0; n < program_count; n++){
        pat->program_number            = m_byte_r8(b) << 8 | m_byte_r8(b);
        bt = m_byte_r8(b);
        pat->reserved_3                = bt >> 5;
        ts->pmts[n].program_number = pat->program_number;
        ts->pmts[n].reserved_3 = pat->reserved_3;

        if ( pat->program_number == 0x0 ){
            pat->network_PID = ((bt & 0x1F) << 8) | m_byte_r8(b);
            ts->pmts[n].network_PID = pat->network_PID;
        }else{
            pat->program_map_PID = ((bt & 0x1F) << 8) | m_byte_r8(b);
            ts->pmts[n].program_map_PID = pat->program_map_PID;
        }
        if(displayInfo){
            printf("[%d/%d] program_number=%d program_map_PID=0x%X\n",
                    n, program_count, pat->program_number, pat->program_map_PID);
        }
    }

    // Get CRC_32
    pat->CRC_32 = m_byte_rb32(b);
    return 0;
}

int
ts_parser_adjust_PMT(TsParser *ts, MByte *b, TS_packet_header* hd)
{
    TS_PMT *pmt = &ts->pmt;
    guint32 i, bt;

    bt = 0;
    //3 Bytes
    pmt->table_id                 = m_byte_r8(b);
    bt = m_byte_r8(b);
    pmt->section_syntax_indicator = bt >> 7;
    pmt->zero                     = bt >> 6;
    pmt->reserved_1               = bt >> 4;
    pmt->section_length           = ((bt & 0x0F) << 8) | m_byte_r8(b);

    //9 Bytes
    pmt->program_number           = m_byte_r8(b) << 8 | m_byte_r8(b);
    bt = m_byte_r8(b);
    pmt->reserved_2               = bt >> 6;
    pmt->version_number           = bt >> 1 & 0x1F;
    pmt->current_next_indicator   = (bt << 7) >> 7;
    pmt->section_number           = m_byte_r8(b);
    pmt->last_section_number      = m_byte_r8(b);
    bt = m_byte_r8(b);
    pmt->reserved_3               = bt >> 5;
    pmt->PCR_PID                  = ((bt << 8) | m_byte_r8(b)) & 0x1FFF;
    bt = m_byte_r8(b);
    pmt->reserved_4               = bt >> 4;
    pmt->program_info_length      = (bt & 0x0F) << 8 | m_byte_r8(b);

    m_byte_skip(b, pmt->program_info_length );
    // Get stream type and PID

    TS_MPMT* m = ts_parser_get_mpat(ts, hd->PID);
    if(m && m->nb_es) { m = NULL; }
    int dsize = pmt->section_length - 9 - pmt->program_info_length - 4;

//    av_print("dsize=%d pmt->section_length=%d pmt->program_info_length=%d\n", dsize, pmt->section_length, pmt->program_info_length);

    for (i = 0 ; i < dsize; ){
        pmt->stream_type      = m_byte_r8(b); //p[i];
        bt = m_byte_r8(b);
        pmt->reserved_5       = bt >> 5; //p[i+1] >> 5;
        pmt->elementary_PID   = ((bt << 8) | m_byte_r8(b)) & 0x1FFF;
        bt = m_byte_r8(b);
        pmt->reserved_6       = bt >> 4;
        pmt->ES_info_length   = (bt & 0x0F) << 8 | m_byte_r8(b);
        // Store in es
        if(m){
            m->es[m->nb_es].es_type = (guint16)pmt->stream_type;
            m->es[m->nb_es].es_pid  = (guint16)pmt->elementary_PID;
            av_print_int(m->es[m->nb_es].es_type);
            av_print_int(m->es[m->nb_es].es_pid);
            m->nb_es ++;
        }

        i += 5 + pmt->ES_info_length;
    }

    if(m){
        av_print("m->nb_es=%d\n", m->nb_es);
    }

    // Get CRC_32
    pmt->CRC_32 = m_byte_rb32(b);

    return 0;
}

int
ts_parser_adjust_PES(TsParser *ts, MByte *b, TS_packet_header* hd)
{
    TS_ADAPTATION *adapta = &ts->adapta;
    PES_HEADER    *pes    = &ts->pes;
    guint64 timestamp;
    int i, res = 0;

    if(hd->adaption_field_control == 2){        //Adaptation_field only, no payload
        //do nothing, return directly
        ts_parser_adjust_adaptation(ts, b, adapta);
        return 0;
    }else if(hd->adaption_field_control == 3){  //Adaptation_field followed by payload
        ts_parser_adjust_adaptation(ts, b, adapta);
    }
    if(hd->payload_unit_start_indicator){
        guint32 id = 0;
        guint32 tb;
        guint8 *p;

//        ts_parser_print_TSHD(hd);

        res = m_byte_find(b, &id, 0x00000100, 0xFFFFFF00);
        if(res < 0) { return res; }
        pes->packet_start_code_prefix = id >> 8;

        assert(pes->packet_start_code_prefix == 1);

        pes->stream_id = id & 0xFF;
        pes->PES_packet_length = m_byte_rb16(b);
        if(    pes->stream_id == PES_PROGRAM_STREAM_MAP
                || pes->stream_id == PES_PRIVATE_STREAM_2
                || pes->stream_id == PES_ECM_STREAM
                || pes->stream_id == PES_EMM_STREAM
                || pes->stream_id == PES_DSMCC_STREAM
                || pes->stream_id == PES_ITU_T_REC_H_222_1)
        {
            return 0;
        }
        tb = m_byte_r8(b);
        pes->mark10 = tb >> 6;
        pes->PES_scrambling_control = (tb >> 4) & 0x3;
        pes->PES_priority = (tb >> 3) & 1;
        pes->data_alignment_indicator = (tb >> 2) & 1;
        pes->copyright = (tb >> 1) & 1;
        pes->original_or_copy = (tb) & 1;

        tb = m_byte_r8(b);
        pes->PTS_DTS_flags = tb >> 6;
        pes->ESCR_flag = (tb >> 5) & 1;
        pes->ES_rate = (tb >> 4) & 1;
        pes->DSM_trick_mode_flag = (tb >> 3) & 1;
        pes->additional_copy_info_flag = (tb >> 2) & 1;
        pes->PES_CRC_flag = (tb >> 1) & 1;
        pes->PES_extension_flag = (tb) & 1;
        pes->PES_header_data_length = m_byte_r8(b);

        if(pes->PTS_DTS_flags & 2){
            for(i = 0; i < 5; i++){
                pes->pts[i] = m_byte_r8(b);
            }
           ts->pts[ts->nb_pts ++] = get_ts(pes->pts);
        }
        if(pes->PTS_DTS_flags & 1){
            for(i = 0; i < 5; i++){
                pes->dts[i] = m_byte_r8(b);
            }
            ts->dts[ts->nb_dts ++] = get_ts(pes->dts);
        }

        if(pes->ESCR_flag == 1){
            //skip 48 bits
            for(i = 0; i < 6; i++){
                pes->ESCR_base[i] = m_byte_r8(b);
            }
        }

        if(pes->ES_rate_flag == 1){
            tb = m_byte_rb24(b);
            pes->marker_bit5 = tb >> 23;
            pes->ES_rate = (tb >> 1) & (0x3FFFFF);
            pes->marker_bit6 = tb & 1;
        }
        if(pes->DSM_trick_mode_flag == 1){
            tb = m_byte_r8(b);
            pes->trick_mode_control = tb >> 5;
            pes->reserved2 = tb & 0x1F;
        }
        if(pes->additional_copy_info_flag == 1){
            tb = m_byte_r8(b);
            pes->marker_bit7 = tb >> 7;
            pes->additional_copy_info = tb & 0x7F;
        }
        if(pes->PES_CRC_flag == 1){
            pes->previous_PES_packet_CRC = m_byte_rb16(b);
        }
        if(pes->PES_extension_flag == 1){
            tb = m_byte_r8(b);
            pes->PES_private_data_flag = tb >> 7;
            pes->pack_header_field_flag = (tb >> 6) & 1;
            pes->program_packet_sequence_counter_flag = (tb >> 5) & 1;
            pes->P_STD_buffer_flag = (tb >> 4) & 1;
            pes->reserved3 = (tb >> 1) & 7;
            pes->PES_extension_flag_2 = tb & 1;

            if(pes->PES_private_data_flag == 1){
                m_byte_skip(b, 128 / 8);
            }
            if(pes->pack_header_field_flag == 1){
                pes->pack_field_length = m_byte_r8(b);
                //FIXME
                m_byte_skip(b, pes->pack_field_length);
            }
            if(pes->program_packet_sequence_counter_flag == 1){
                tb = m_byte_r8(b);
                pes->marker_bit8 = tb >> 7;
                pes->program_packet_sequence_counter = tb & 0x7F;
                tb = m_byte_r8(b);
                pes->marker_bit9 = tb >> 7;
                pes->MPEG1_MPEG2_identifier = (tb >> 6) & 1;
                pes->original_stuff_length = tb & 0x3F;
            }
            if(pes->P_STD_buffer_flag){
                tb = m_byte_rb16(b);
                pes->marker_bit10 = tb >> 14;
                pes->P_STD_buffer_scale = (tb >> 13) & 1;
                pes->P_STD_buffer_size = tb & 0x1FFF;
            }
            if(pes->PES_extension_flag_2 == 1){
                tb = m_byte_r8(b);
                pes->marker_bit11 = tb >> 7;
                pes->PES_extension_field_length = tb & 0x7F;
                tb = m_byte_r8(b);
                pes->stream_id_extension_flag = tb >> 7;
                if(pes->stream_id_extension_flag == 0){
                    pes->stream_id_extension = tb & 0x7F;
                    // the one byte is last byte
                    m_byte_skip(b, pes->PES_extension_field_length - 1);
                }
            }
        }

    }else{
        //fwrite(b.getBufPtr(), d->mPacketSize - 4, 1, fp);
    }
}

int
ts_parser_adjust_adaptation(TsParser *ts, MByte *b, TS_ADAPTATION* af)
{
    guint32 bt = 0;
    // adaptation field length
    guint8 *head = b->mBufPtr;
    af->adaptation_field_length = m_byte_r8(b);

    if (af->adaptation_field_length == 0) {
        //af->raw_data = buf;
        af->raw_data_size = 1;
        //DEMUX_DEBUG("af->adaptation_field_length = 0 : %d\n", af->adaptation_field_length);
        return MOK;
    }

    bt = m_byte_r8(b);
    af->discontinuity_indicator = (bt >> 7) & 1;
    af->random_access_indicator = (bt >> 6) & 1;
    af->elementary_stream_priority_indicator = (bt >> 5) & 1;
    af->pcr_flag = (bt >> 4) & 1;
    af->opcr_flag = (bt >> 3) & 1;
    af->splicing_point_flag = (bt >> 2) & 1;
    af->transport_private_data_flag = (bt >> 1) & 1;
    af->adaptation_field_extension_flag = (bt & 0x01);

    // program clock
    if (af->pcr_flag == 0x01) {
        guint64 v1 = m_byte_rb24(b);
        bt = m_byte_rb24(b);
        af->program_clock_reference_base = ((v1 << 9) | (bt >> 15) );
        af->pcr_reserved = (bt >> 9) & 0x3F;
        af->program_clock_reference_extension = (bt & 0x1FF);
    }

    // original program clock
    if (af->opcr_flag == 0x01) {
        guint64 v1 = m_byte_rb24(b);
        bt = m_byte_rb24(b);
        af->original_program_clock_reference_base = ((v1 << 9) | (bt >> 15) );
        af->opcr_reserved = (bt >> 9) & 0x3F;
        af->original_program_clock_reference_extension = (bt & 0x1FF);
    }

    // splicing point flag
    if (af->splicing_point_flag == 0x01) {
        af->splice_countdown = m_byte_r8(b);
    }

    // transport private data flag
    if (af->transport_private_data_flag == 0x01) {
        af->transport_private_data_length = m_byte_r8(b);

        //af->private_data_byte = &p[index];
        m_byte_skip(b, af->transport_private_data_length);
    }

    // adaptation field extension flag
    if (af->adaptation_field_extension_flag == 0x01) {
        af->adaptation_field_extension_length = m_byte_r8(b);

        bt = m_byte_r8(b);

        af->ltw_flag = (bt >> 7) & 1;
        af->piecewise_rate_flag = (bt >> 6) & 1;
        af->seamless_splice_flag = (bt >> 5) & 1;
        af->adaptation_field_extension_reserved1 = bt & 0x1F;

        // ltw flag
        if (af->ltw_flag == 0x01) {
            bt = m_byte_rb16(b);
            af->ltw_valid_flag = bt >> 15;
            af->ltw_offset = (bt & 0x7FFF);
        }

        // piecewise rate flag
        if (af->piecewise_rate_flag == 0x01) {
            bt = m_byte_rb24(b);
            af->piecewise_rate_reserved = bt >> 22;
            af->piecewise_rate = ( bt & 0x3FFFFF);
        }

        // seamless splice flag
        if (af->seamless_splice_flag == 0x01) {
            bt = m_byte_r8(b);
            af->splice_type = (bt >> 4) & 0x0F;
            af->dts_next_au1 = (bt >> 1) & 0x07;
            af->seamless_splice_marker_bit1 = bt & 0x01;

            bt = m_byte_rb16(b);
            af->dts_next_au2 = (bt >> 1) & 0x7FFF;
            af->seamless_splice_marker_bit2 = bt & 0x01;

            bt = m_byte_rb16(b);
            af->dts_next_au3 = (bt >> 1) & 0x7FFF;
            af->seamless_splice_marker_bit3 = bt & 0x01;
        }
    }
    //add 1 byte because the adaptation_field_length not include oneself
    int remained = af->adaptation_field_length + 1 - (b->mBufPtr - head);
    assert(remained >= 0);
//    av_print("remained=%d\n", remained);
    if(remained > 0) { m_byte_skip(b, remained); }

    return 0;
}


TS_MPMT*
ts_parser_get_mpat(TsParser *ts, int pid)
{
    int i;
    TS_MPMT* m = NULL;
    for(i = 0; i < ts->nb_pmts; i++){
        if(ts->pmts[i].program_map_PID == pid){
            m = &ts->pmts[i];
            break;
        }
    }
    return m;
}

int
ts_parser_isPMT_PID(TsParser *ts, int pid){
    gboolean isPid = FALSE;
    int i;

    for(i = 0; i < ts->nb_pmts; i++){
        if(ts->pmts[i].program_map_PID == pid){
            isPid = TRUE;
            break;
        }
    }
    return isPid;
}

int
ts_parser_isPES_PID(TsParser *ts, int pid)
{
    int i, j;

    for(i = 0; i < ts->nb_pmts; i++){
        for(j = 0; j < ts->pmts[i].nb_es; j++){
            if(ts->pmts[i].es[j].es_pid == pid){
                return TRUE;
            }
        }
    }
    return FALSE;
}

void
ts_parser_print_TSHD(TS_packet_header* hd)
{
    av_print_int(hd->sync_byte);
    av_print_int(hd->transport_error_indicator);
    av_print_int(hd->payload_unit_start_indicator);
    av_print_int(hd->transport_priority);
    av_print_int(hd->PID);
    av_print_int(hd->transport_scrambling_control);
    av_print_int(hd->adaption_field_control);
    av_print_int(hd->continuity_counter);
}




