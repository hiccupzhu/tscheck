/*
 * mbit.c
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#include "mbit.h"

G_DEFINE_TYPE (MBit, m_bit, G_TYPE_OBJECT)

struct _MBitPrivate
{
    int temp;
};

#define REFIX_BIT_GET_PRIVATE(obj) \
    (G_TYPE_INSTANCE_GET_PRIVATE ((obj), M_TYPE_BIT, MBitPrivate))

static void m_bit_dispose(GObject *object);
static void m_bit_finalize(GObject *object);
static void m_bit_constructed (GObject *object);

static
void m_bit_class_init (MBitClass *klass){
    GObjectClass *base_class = G_OBJECT_CLASS (klass);

    g_print("[%s::%d::%s]\n", __FILE__, __LINE__, __func__);

    base_class->constructed  = m_bit_constructed;
    base_class->dispose      = m_bit_dispose;
    base_class->finalize     = m_bit_finalize;

    g_type_class_add_private (klass, sizeof(MBitPrivate));
}

static
void m_bit_init (MBit *self){
    MBitPrivate *priv;

    g_print("[%s::%d::%s]\n", __FILE__, __LINE__, __func__);

    self->priv = priv = REFIX_BIT_GET_PRIVATE (self);
}

static void
m_bit_constructed (GObject *object)
{
    MBit *self = M_BIT(object);
}

static
void m_bit_dispose(GObject *object){
    MBit *self = M_BIT(object);
    g_print("[%s::%d::%s]\n", __FILE__, __LINE__, __func__);

    G_OBJECT_CLASS(m_bit_parent_class)->dispose(object);
}

static
void m_bit_finalize(GObject *object){
    MBit *self = M_BIT(object);
    g_print("[%s::%d::%s]\n", __FILE__, __LINE__, __func__);
    G_OBJECT_CLASS(m_bit_parent_class)->finalize(object);
}

MBit *
m_bit_new (void)
{
    return g_object_new(M_TYPE_BIT, NULL);
}

void
m_bit_do_action (MBit *self)
{
    g_return_if_fail(M_IS_BIT(self));

}

