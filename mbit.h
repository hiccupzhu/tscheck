/*
 *  mbit.h
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#ifndef _M_BIT_H_
#define _M_BIT_H_

#include <glib-object.h>

#define M_TYPE_BIT               (m_bit_get_type ())
#define M_BIT(object)            (G_TYPE_CHECK_INSTANCE_CAST ((object), M_TYPE_BIT, MBit))
#define M_IS_BIT(object)         (G_TYPE_CHECK_INSTANCE_TYPE ((object), M_TYPE_BIT))
#define M_BIT_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), M_TYPE_BIT, MBitClass))
#define M_IS_BIT_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), M_TYPE_BIT))
#define M_BIT_GET_CLASS(object)  (G_TYPE_INSTANCE_GET_CLASS ((object), M_TYPE_BIT, MBitClass))

typedef struct _MBit          MBit;
typedef struct _MBitClass     MBitClass;
typedef struct _MBitPrivate   MBitPrivate;

struct _MBit {
    GObject parent;

    MBitPrivate *priv;
};

struct _MBitClass {
    GObjectClass parent_class;
};

GType m_bit_get_type (void);
MBit *m_bit_new (void);
void m_bit_do_action (MBit *self);

#endif
