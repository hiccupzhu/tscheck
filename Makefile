CC= gcc
CFLAGS=$(shell pkg-config --cflags gobject-2.0 gthread-2.0) -g
LFLAGS=$(shell pkg-config --libs gobject-2.0 gthread-2.0) -lpcap -lpthread

OBJS=tscheck.o tsreader.o tsparser.o mbyte.o mbuf.o mbit.o

tscheck:$(OBJS)
	$(CC) -o $@ $+ $(LFLAGS)

%.o:%.c %.h
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -rf *.o tscheck