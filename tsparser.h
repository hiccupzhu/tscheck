/*
 *  tsparser.h
 *
 *  Created on: Apr 12, 2013
 *      Author: szhu
 */

#ifndef _TS_PARSER_H_
#define _TS_PARSER_H_

#include <glib-object.h>

#include "mbuf.h"
#include "mbyte.h"

typedef struct TS_packet_header{
    guint32 sync_byte                    : 8;
    guint32 transport_error_indicator    : 1;
    guint32 payload_unit_start_indicator : 1;
    guint32 transport_priority           : 1;
    guint32 PID                          : 13;
    guint32 transport_scrambling_control : 2;
    guint32 adaption_field_control       : 2;
    guint32 continuity_counter           : 4;
} TS_packet_header;

typedef struct TS_PAT{
    guint32 table_id                       : 8;
    guint32 section_syntax_indicator       : 1;
    guint32 zero                           : 1;
    guint32 reserved_1                     : 2;
    guint32 section_length                 : 12;
    guint32 transport_stream_id            : 16;
    guint32 reserved_2                     : 2;
    guint32 version_number                 : 5;
    guint32 current_next_indicator         : 1;
    guint32 section_number                 : 8;
    guint32 last_section_number            : 8;
    guint32 program_number                 : 16;
    guint32 reserved_3                     : 3;
    guint32 network_PID                    : 13;
    guint32 program_map_PID                : 13;
    guint32 CRC_32                         : 32;
} TS_PAT;

typedef struct {
    int stream_type;
//    MAJOR_MEDIA_TYPE codec_type;
//    enum CodecID codec_id;
    const char* descriptor;
}STREAM_TYPE;

typedef struct TS_MPMT{
    guint32 program_number                 : 16;
    guint32 reserved_3                     : 3;
    union{
        guint32 network_PID                : 13;
        guint32 program_map_PID            : 13;
    };
    guint8   nb_es;
    struct ESINFO{
        guint16  es_type;
        guint16  es_pid;
        const STREAM_TYPE* stream_type;
    }es[8];
}TS_MPMT;

typedef struct TS_PMT
{
    guint32 table_id                       : 8;
    guint32 section_syntax_indicator       : 1;
    guint32 zero                           : 1;
    guint32 reserved_1                     : 2;
    guint32 section_length                 : 12;
    guint32 program_number                 : 16;
    guint32 reserved_2                     : 2;
    guint32 version_number                 : 5;
    guint32 current_next_indicator         : 1;
    guint32 section_number                 : 8;
    guint32 last_section_number            : 8;
    guint32 reserved_3                     : 3;
    guint32 PCR_PID                        : 13;
    guint32 reserved_4                     : 4;
    guint32 program_info_length            : 12;

    guint32 stream_type                    : 8;
    guint32 reserved_5                     : 3;
    guint32 elementary_PID                 : 13;
    guint32 reserved_6                     : 4;
    guint32 ES_info_length                 : 12;
    guint32 CRC_32                         : 32;
} TS_PMT;

typedef struct TS_ADAPTATION {
    guint8 adaptation_field_length               : 8;
    guint8 discontinuity_indicator               : 1;
    guint8 random_access_indicator               : 1;
    guint8 elementary_stream_priority_indicator  : 1;
    guint8 pcr_flag                              : 1;
    guint8 opcr_flag                             : 1;
    guint8 splicing_point_flag                   : 1;
    guint8 transport_private_data_flag           : 1;
    guint8 adaptation_field_extension_flag       : 1;
    guint64 program_clock_reference_base         : 33;
    guint8 pcr_reserved                          : 6;
    guint16 program_clock_reference_extension    : 9;
    guint64 original_program_clock_reference_base: 33;
    guint8 opcr_reserved                         : 6;
    guint16 original_program_clock_reference_extension : 9;
    guint8 splice_countdown                      : 8;
    guint8 transport_private_data_length         : 8;
    //data ...
    guint8 adaptation_field_extension_length     : 8;
    guint8 ltw_flag                              : 1;
    guint8 piecewise_rate_flag                   : 1;
    guint8 seamless_splice_flag                  : 1;
    guint8 adaptation_field_extension_reserved1  : 5;
    guint8 ltw_valid_flag                        : 1;
    guint16 ltw_offset                           : 15;
    guint8 piecewise_rate_reserved               : 2;
    guint32 piecewise_rate                       : 22;
    guint8 splice_type                           : 4;
    guint8 dts_next_au1                          : 3;
    guint8 seamless_splice_marker_bit1           : 1;
    guint16 dts_next_au2                         : 15;
    guint8 seamless_splice_marker_bit2           : 1;
    guint16 dts_next_au3                         : 15;
    guint8 seamless_splice_marker_bit3           : 1;
    /*guint8* adaptation_field_extension_reserved2;*/
//     guint8 adaptation_field_extension_reserved2_length;
//     guint8* stuffing_byte;
//     guint8 stuffing_byte_length;
//
    guint8* raw_data;
    guint32 raw_data_size;
} TS_ADAPTATION;

typedef struct{
    // Basic part, mandatory 9 bytes
    guint32 packet_start_code_prefix : 24;
    guint32 stream_id : 8;
    //guint8 start_code[4]; // 0x000001XX
    guint16 PES_packet_length;

    guint8 original_or_copy          : 1;
    guint8 copyright                 : 1; // 0
    guint8 data_alignment_indicator  : 1; // 0
    guint8 PES_priority              : 1; // 0
    guint8 PES_scrambling_control    : 2;
    guint8 mark10                    : 2; // 10b

    guint8 PES_extension_flag        : 1;
    guint8 PES_CRC_flag              : 1;
    guint8 additional_copy_info_flag : 1;
    guint8 DSM_trick_mode_flag       : 1;
    guint8 ES_rate_flag              : 1;
    guint8 ESCR_flag                 : 1;
    guint8 PTS_DTS_flags             : 2;

    guint8 PES_header_data_length ;

    guint8 pts[5];
    guint8 dts[5];
//     guint8 mark_pts : 4;
//     guint8 pts1     : 3;
//     guint8 marker_pts1 : 1;
//     guint16 pts2    : 15;
//     guint16 marker_pts2 : 1;
//     guint16 pts3    : 15;
//     guint16 marker_pts3 : 1;
//
//     guint8 mark_dts : 4;
//     guint8 dts1     : 3;
//     guint8 marker_dts1 : 1;
//     guint16 dts2    : 15;
//     guint16 marker_dts2 : 1;
//     guint16 dts3    : 15;
//     guint16 marker_dts3 : 1;

    guint8 ESCR_base[6];
//     guint16 reserved1    : 2;
//     guint16 ESCR_base1  : 3;
//     guint16 marker_bit1 : 1;
//     guint16 ESCR_base2  : 15;
//     guint16 marker_bit2 : 1;
//     guint16 ESCR_base3  : 15;
//     guint16 marker_bit3 : 1;
//     guint16 ESCR_extension : 9;
//     guint16 marker_bit4 : 1;

    guint32 marker_bit5 : 1;
    guint32 ES_rate     : 22;
    guint32 marker_bit6 : 1;

    guint8  trick_mode_control : 3;
    guint8  reserved2 : 5;

    guint8  marker_bit7 : 1;
    guint8  additional_copy_info : 7;

    guint16 previous_PES_packet_CRC ;

    guint8  PES_private_data_flag : 1;
    guint8  pack_header_field_flag : 1;
    guint8  program_packet_sequence_counter_flag : 1;
    guint8  P_STD_buffer_flag : 1;
    guint8  reserved3 : 3;
    guint8  PES_extension_flag_2 : 1;

    //guint8 PES_private_data*;
    guint8  pack_field_length;
    guint8  marker_bit8 : 1;
    guint8  program_packet_sequence_counter : 7;
    guint8  marker_bit9 : 1;
    guint8  MPEG1_MPEG2_identifier : 1;
    guint8  original_stuff_length : 6;

    guint16 marker_bit10 : 2;
    guint16 P_STD_buffer_scale : 1;
    guint16 P_STD_buffer_size : 13;

    guint8  marker_bit11  : 1;
    guint8  PES_extension_field_length : 7;
    guint8  stream_id_extension_flag : 1;
    guint8  stream_id_extension : 7;

} PES_HEADER;


enum {
    PES_PROGRAM_STREAM_MAP = 0xBC,
    PES_PRIVATE_STREAM_1   = 0xBD,
    PES_ADDING_STREAM      = 0xBE,
    PES_PRIVATE_STREAM_2   = 0xBF,
    PES_AUDIO_MASK         = 0xE0,
    PES_AUDIO_ID           = 0xC0,
    PES_VIDEO_MASK         = 0xF0,
    PES_VIDEO_ID           = 0xE0,
    PES_ECM_STREAM         = 0xF0,
    PES_EMM_STREAM         = 0xF1,
    PES_DSMCC_STREAM       = 0xF2,
    PES_IEC_13522_STREAM   = 0xF3,
    PES_ITU_T_REC_H_222_1  = 0xF4,
};

enum {
    TS_PID_PAT       = 0x0000,
    TS_PID_CAT       = 0x0001,
    TS_PID_TSDT      = 0x0002,
    //reserved    0x0003-0x000F
    TS_PID_NIT_ST    = 0x0010,
    TS_PID_SDT_BAT_ST= 0x0011,
    TS_PID_EIT_ST    = 0x0012,
    TS_PID_RST_ST    = 0x0013,
    TS_PID_TDT_TOT_ST= 0x0014,
    //network-sync    0x0015
    //reserved    0x0016-0x001B
    //signaling in band    0x001C
    //measure          0x001D
    TS_PID_DIT       = 0x001E,
    TS_PID_SIT       = 0x001F,
    TS_PID_NULL      = 0x1FFF
};

#define MAX_TIMESTAMP 10

#define TS_TYPE_PARSER               (ts_parser_get_type ())
#define TS_PARSER(object)            (G_TYPE_CHECK_INSTANCE_CAST ((object), TS_TYPE_PARSER, TsParser))
#define TS_IS_PARSER(object)         (G_TYPE_CHECK_INSTANCE_TYPE ((object), TS_TYPE_PARSER))
#define TS_PARSER_CLASS(klass)       (G_TYPE_CHECK_CLASS_CAST ((klass), TS_TYPE_PARSER, TsParserClass))
#define TS_IS_PARSER_CLASS(klass)    (G_TYPE_CHECK_CLASS_TYPE ((klass), TS_TYPE_PARSER))
#define TS_PARSER_GET_CLASS(object)  (G_TYPE_INSTANCE_GET_CLASS ((object), TS_TYPE_PARSER, TsParserClass))

typedef struct _TsParser          TsParser;
typedef struct _TsParserClass     TsParserClass;
typedef struct _TsParserPrivate   TsParserPrivate;

struct _TsParser {
    GObject parent;

    TsParserPrivate *priv;

    int             nb_pmts;
    TS_MPMT         *pmts;

    MByte           *b;
    int             mPacketSize;

    TS_PAT          pat;
    TS_PMT          pmt;
    TS_ADAPTATION   adapta;
    PES_HEADER      pes;
    TS_packet_header tshd;

    gint            nb_pts;
    guint64         pts[MAX_TIMESTAMP];
    gint            nb_dts;
    guint64         dts[MAX_TIMESTAMP];
    gint            nb_pcr;
    guint64         pcr[MAX_TIMESTAMP];
};

struct _TsParserClass {
    GObjectClass parent_class;
};

GType       ts_parser_get_type (void);
TsParser *  ts_parser_new (void);
int         ts_parser_write(TsParser *ts, MBuf *buf);
int         ts_parser_clear(TsParser *ts);

int         ts_parser_adjust_TSPacketHeader(TsParser *ts, MByte *b, TS_packet_header *hd);
int         ts_parser_adjust_packet_size(TsParser *ts, const guint8* buf, int size, int* pktSize);
gboolean    ts_parser_has_valid_PMT(TsParser *ts);
gboolean    ts_parser_anchor_start(TsParser *ts, MByte *byte, int pktSize);
int         ts_parser_adjust_PAT(TsParser *ts, MByte *b, TS_packet_header* hd);
int         ts_parser_adjust_PMT(TsParser *ts, MByte *b, TS_packet_header* hd);
int         ts_parser_adjust_PES(TsParser *ts, MByte *b, TS_packet_header* hd);
int         ts_parser_adjust_adaptation(TsParser *ts, MByte *b, TS_ADAPTATION* hd);
TS_MPMT*    ts_parser_get_mpat(TsParser *ts, int pid);
int         ts_parser_isPMT_PID(TsParser *ts, int pid);
int         ts_parser_isPES_PID(TsParser *ts, int pid);

void        ts_parser_print_TSHD(TS_packet_header* hd);

static guint64 get_ts(guint8* p){
    return ((guint64)(p[0] & 0x0E) << 29) | (p[1] << 22) | ((p[2] & 0xFE) << 14) | (p[3] << 7) | ((p[4] & 0xFE) >> 1);
}

#endif
