/*
 * tscheck.c
 *
 *  Created on: Apr 13, 2013
 *      Author: szhu
 */


#include <glib.h>
#include <unistd.h>
#include "gdef.h"
#include "tsreader.h"
#include "tsparser.h"

static char* interface = "em1";
//"udp and dst host 239.100.191.3 and dst port 1234"
static char* option = "";
static char* check_timestamp="";

void
tscheck_usage()
{
    printf(
            "tscheck [options]\n"
            "       -i interface [default: em1]\n"
            "       -o option\n"
            "       -c pts##dts\n"
            "       -h help\n"
            "e.g.\n"
            "   ./tscheck -i em3 -c dts -o \"udp and dst host 239.100.191.3 and dst port 1234\"\n"
            "\n"
            );
}

static void
parse_args(int argc, char* argv[])
{
    char ch;
    while((ch=getopt(argc,argv,"i:hc:o:"))!=-1)
    {
//        printf("optind:%d\n",optind);
//        printf("optarg:%s\n",optarg);
        switch(ch)
        {
        case 'h':
            tscheck_usage();
            exit(0);
            break;
        case 'i':
            interface = strdup(optarg);
            break;
        case 'o':
            option = strdup(optarg);
            break;
        case 'c':
            check_timestamp = strdup(optarg);
            break;
        default:
            printf("other option:%c\n",ch);
            exit(1);
        }
    }
}

int main(int argc, char* argv[]){
    int i, j, ret;
    MBuf *buf;
    guint64 last_pts, last_dts;
    TsReader *reader;
    TsParser *ts;
    FILE* fp = NULL;

    g_type_init ();
    g_thread_init(NULL);

    parse_args(argc, argv);

    reader = ts_reader_new();
    ts = ts_parser_new();

    av_print("interface=%s option=%s\n", interface, option);

    ret = ts_reader_open(reader, interface, option);
    if(!ret) return -1;

    while(1){
        buf = ts_reader_read(reader);
        if(!buf){
//            av_printf("ts_reader_read can not read any data!\n");
            continue;
        }
//        av_print("buf->size=%d\n", buf->size);

//        fp = fopen("/opt/ts_7.ts", "wb");
//        fwrite(buf->data, 1, buf->size, fp);
//        fclose(fp);

        ts_parser_write(ts, buf);

        if(strstr(check_timestamp, "pts")){
            for(i = 0; i < ts->nb_pts; i++){
                if(ts->pts[i] < last_pts){
                    g_printf("ERRO PTS [%.2f]%.2f \n", i, ts->nb_pts, (last_pts - ts->pts[i]) / 90.0, ts->pts[i] / 90.0);
                }
                last_pts = ts->pts[i];
            }
        }else if(strstr(check_timestamp, "dts")){
            for(i = 0; i < ts->nb_dts; i++){
                if(ts->dts[i] <= last_dts)
                {
                    g_printf("ERRO DTS [%.2f]%.2f \n", i, ts->nb_dts, (last_dts - ts->dts[i]) / 90.0, ts->dts[i] / 90.0);
                }
                last_dts = ts->dts[i];
            }
        }

        g_object_unref(buf);
    }


    g_object_unref(ts);
    g_object_unref(reader);

    return 0;
}




